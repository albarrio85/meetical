package com.meetical.ingestion.microsoft;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Value {
   @SerializedName("subject")
   @Expose
   private String subject;
   @SerializedName("start")
   @Expose
   private Start start;
   @SerializedName("attendees")
   @Expose
   private List<Attendee> attendees = null;

   public String getSubject() {
      return subject;
   }

   public void setSubject(String subject) {
      this.subject = subject;
   }

   public Start getStart() {
      return start;
   }

   public void setStart(Start start) {
      this.start = start;
   }

   public List<Attendee> getAttendees() {
      return attendees;
   }

   public void setAttendees(List<Attendee> attendees) {
      this.attendees = attendees;
   }
}
