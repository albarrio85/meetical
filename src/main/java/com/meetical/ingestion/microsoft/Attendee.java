package com.meetical.ingestion.microsoft;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attendee {

   @SerializedName("emailAddress")
   @Expose
   private EmailAddress emailAddress;

   public EmailAddress getEmailAddress() {
      return emailAddress;
   }

   public void setEmailAddress(EmailAddress emailAddress) {
      this.emailAddress = emailAddress;
   }
}
