package com.meetical.ingestion.microsoft;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MicrosoftEventData {

   @SerializedName("value")
   @Expose
   private List<Value> value = null;

   public List<Value> getValue() {
      return value;
   }

   public void setValue(List<Value> value) {
      this.value = value;
   }

   @Override
   public String toString() {
      final StringBuffer sb = new StringBuffer("MicrosoftEventData{");
      sb.append("value=").append(value);
      sb.append('}');
      return sb.toString();
   }
}
