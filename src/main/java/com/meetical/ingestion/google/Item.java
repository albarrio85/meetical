package com.meetical.ingestion.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {

   @SerializedName("summary")
   @Expose
   private String summary;

   @SerializedName("start")
   @Expose
   private Start start;

   @SerializedName("attendees")
   @Expose
   private List<Attende> attendees = null;

   public String getSummary() {
      return summary;
   }

   public void setSummary(String summary) {
      this.summary = summary;
   }

   public Start getStart() {
      return start;
   }

   public void setStart(Start start) {
      this.start = start;
   }

   public List<Attende> getAttendees() {
      return attendees;
   }

   public void setAttendees(List<Attende> attendees) {
      this.attendees = attendees;
   }

   @Override
   public String toString() {
      final StringBuffer sb = new StringBuffer("Item{");
      sb.append("summary='").append(summary).append('\'');
      sb.append(", start=").append(start);
      sb.append(", attendees=").append(attendees);
      sb.append('}');
      return sb.toString();
   }
}
