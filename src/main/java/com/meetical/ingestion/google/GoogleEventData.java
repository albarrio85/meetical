package com.meetical.ingestion.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GoogleEventData {

   @SerializedName("kind")
   @Expose
   private String kind;

   @SerializedName("summary")
   @Expose
   private String summary;

   @SerializedName("timeZone")
   @Expose
   private String timeZone;

   @SerializedName("items")
   @Expose
   private List<Item> items = null;

   public String getKind() {
      return kind;
   }

   public void setKind(String kind) {
      this.kind = kind;
   }

   public String getSummary() {
      return summary;
   }

   public void setSummary(String summary) {
      this.summary = summary;
   }

   public String getTimeZone() {
      return timeZone;
   }

   public void setTimeZone(String timeZone) {
      this.timeZone = timeZone;
   }

   public List<Item> getItems() {
      return items;
   }

   public void setItems(List<Item> items) {
      this.items = items;
   }

   @Override
   public String toString() {
      final StringBuffer sb = new StringBuffer("GoogleEventData{");
      sb.append("kind='").append(kind).append('\'');
      sb.append(", summary='").append(summary).append('\'');
      sb.append(", timeZone='").append(timeZone).append('\'');
      sb.append(", items=").append(items);
      sb.append('}');
      return sb.toString();
   }
}
