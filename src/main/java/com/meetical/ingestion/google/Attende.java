package com.meetical.ingestion.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attende {

   @SerializedName("email")
   @Expose
   private String email;

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   @Override
   public String toString() {
      final StringBuffer sb = new StringBuffer("Attende{");
      sb.append("email='").append(email).append('\'');
      sb.append('}');
      return sb.toString();
   }
}
