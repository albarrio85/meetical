package com.meetical.ingestion.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Start {

   @SerializedName("dateTime")
   @Expose
   private String dateTime;

   @SerializedName("timeZone")
   @Expose
   private String timeZone;

   public String getDateTime() {
      return dateTime;
   }

   public void setDateTime(String dateTime) {
      this.dateTime = dateTime;
   }

   public String getTimeZone() {
      return timeZone;
   }

   public void setTimeZone(String timeZone) {
      this.timeZone = timeZone;
   }

   @Override
   public String toString() {
      final StringBuffer sb = new StringBuffer("Start{");
      sb.append("dateTime='").append(dateTime).append('\'');
      sb.append(", timeZone='").append(timeZone).append('\'');
      sb.append('}');
      return sb.toString();
   }
}
