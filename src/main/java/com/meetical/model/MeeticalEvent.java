package com.meetical.model;

public class MeeticalEvent {

    public Event event;
    // email of owner. ES. Anna, Google.summary
    public String calendarId;
// es. Google or Microsoft
    public String calendarProvider;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public String getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(String calendarId) {
        this.calendarId = calendarId;
    }

    public String getCalendarProvider() {
        return calendarProvider;
    }

    public void setCalendarProvider(String calendarProvider) {
        this.calendarProvider = calendarProvider;
    }
}
