package com.meetical.model;

public class Attendee {

    public String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Attendee{");
        sb.append("email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
