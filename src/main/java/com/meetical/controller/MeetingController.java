package com.meetical.controller;

import com.meetical.model.MeeticalEvent;
import com.meetical.service.MeetingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/")
public class MeetingController {

   MeetingService meetingService;

   public MeetingController(MeetingService meetingService) {
      this.meetingService = meetingService;
   }

   @GetMapping({"/getAll/{user}"})
   public ResponseEntity<List<MeeticalEvent>> getEvent(@PathVariable String user) {
      List<MeeticalEvent> events = meetingService.getAllByUser(user);
      if (events == null)
         return new ResponseEntity<>(events, HttpStatus.NOT_FOUND);
      return new ResponseEntity<>(events, HttpStatus.OK);
   }

}
