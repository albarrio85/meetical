package com.meetical.service;

import com.meetical.model.MeeticalEvent;

import java.util.List;

public interface GoogleService {

   List<MeeticalEvent> getAll();

}
