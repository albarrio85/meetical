package com.meetical.service;

import com.meetical.model.MeeticalEvent;

import java.util.List;

public interface MicrosoftService {

   List<MeeticalEvent> getAll();
}
