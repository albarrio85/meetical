package com.meetical.service;

import com.meetical.model.MeeticalEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MeetingServiceImpl implements MeetingService {

   @Autowired
   private GoogleServiceImpl googleService;

   @Autowired
   private MicrosoftServiceImpl microsoftService;

   @Override
   public List<MeeticalEvent> getAllByUser(String user) {
      if (user.equalsIgnoreCase("anna")){
         return googleService.getAll();
      } else if (user.equalsIgnoreCase("jhon")){
         return microsoftService.getAll();
      }
      return null;
   }

}
