package com.meetical.service;

import com.google.gson.Gson;
import com.meetical.ingestion.google.Attende;
import com.meetical.ingestion.google.GoogleEventData;
import com.meetical.ingestion.google.Item;
import com.meetical.model.Attendee;
import com.meetical.model.Event;
import com.meetical.model.MeeticalEvent;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoogleServiceImpl implements GoogleService {

   List<MeeticalEvent> meeticalEventList = null;

   @Override
   public List<MeeticalEvent> getAll() {
      if (meeticalEventList == null) {
         try {
            // create Gson instance
            Gson gson = new Gson();

            // create a reader
            Reader reader = Files.newBufferedReader(Paths.get("google-event-data.json"));

            // convert JSON string to User object
            GoogleEventData googleEventData = gson.fromJson(reader, GoogleEventData.class);

            // print user object
//            System.out.println(googleEventData);

            meeticalEventList = generateList(googleEventData);

            // close reader
            reader.close();

         } catch (Exception ex) {
            System.out.println("ERROR importing Google Data");
            ex.printStackTrace();
         }

      }

      return meeticalEventList;
   }

   private List<MeeticalEvent> generateList(GoogleEventData googleEventData) {
//      summary: "anna.berlin.meetical@gmail.com" >> ID
      List<MeeticalEvent> list = new ArrayList<>();

      String summary = googleEventData.getSummary();
      String provider = "GOOGLE";
      List<Item> items = googleEventData.getItems().stream()
         .filter(item -> item.getSummary()!=null)
         .sorted(Comparator.comparing(item -> item.getStart().getDateTime()))
         .collect(Collectors.toList());
      if (items != null){
         for (Item item : items) {
            MeeticalEvent meeticalEvent = new MeeticalEvent();
            meeticalEvent.setCalendarId(summary);
            meeticalEvent.setCalendarProvider(provider);
            Event event = new Event();
            event.setSummary(item.getSummary());
            if (item.getStart() != null){
               event.setStartDateTime(item.getStart().getDateTime());
            }
            if (item.getAttendees()!=null){
               List<Attendee> attendeesIn = new ArrayList<>();
               for (Attende attendee : item.getAttendees()){
                  Attendee newElement = new Attendee();
                  newElement.setEmail(attendee.getEmail());
                  attendeesIn.add(newElement);
               }
               event.setAttendees(attendeesIn);
            }
            meeticalEvent.setEvent(event);
            list.add(meeticalEvent);
         }
      }
      return list;
   }
}
