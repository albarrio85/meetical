package com.meetical.service;

import com.google.gson.Gson;
import com.meetical.ingestion.google.Attende;
import com.meetical.ingestion.google.Item;
import com.meetical.ingestion.microsoft.MicrosoftEventData;
import com.meetical.ingestion.microsoft.Value;
import com.meetical.model.Attendee;
import com.meetical.model.Event;
import com.meetical.model.MeeticalEvent;
import org.springframework.stereotype.Service;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MicrosoftServiceImpl implements MicrosoftService {

   List<MeeticalEvent> meeticalEventList = null;

   @Override
   public List<MeeticalEvent> getAll() {
      if (meeticalEventList == null) {
         try {
            // create Gson instance
            Gson gson = new Gson();

            // create a reader
            Reader reader = Files.newBufferedReader(Paths.get("microsoft-event-data.json"));

            // convert JSON string to User object
            MicrosoftEventData googleEventData = gson.fromJson(reader, MicrosoftEventData.class);

            meeticalEventList = generateList(googleEventData);

            // close reader
            reader.close();

         } catch (Exception ex) {
            System.out.println("ERROR importing Microsoft Data");
            ex.printStackTrace();
         }

      }

      return meeticalEventList;
   }

   private List<MeeticalEvent> generateList(MicrosoftEventData microsoftEventData) {
//      summary: "anna.berlin.meetical@gmail.com" >> ID
      List<MeeticalEvent> list = new ArrayList<>();

      //String summary = microsoftEventData.g();
      String provider = "MICROSOFT";
      List<Value> allItems = microsoftEventData.getValue()
         .stream()
         .sorted(Comparator.comparing(item ->item.getStart().getDateTime()))
         .collect(Collectors.toList());

      if (allItems != null){
         for (Value singleItem : allItems) {
            MeeticalEvent meeticalEvent = new MeeticalEvent();
            meeticalEvent.setCalendarProvider(provider);
            Event event = new Event();
            event.setSummary(singleItem.getSubject());
            if (singleItem.getStart() != null){
               event.setStartDateTime(singleItem.getStart().getDateTime());
            }
            if (singleItem.getAttendees()!=null){
               List<Attendee> attendeesIn = new ArrayList<>();
               List<com.meetical.ingestion.microsoft.Attendee> microSoftAttendees = singleItem.getAttendees();
               if (microSoftAttendees != null) {
                  for (com.meetical.ingestion.microsoft.Attendee microsoftAttendee : microSoftAttendees){
                     if (microsoftAttendee.getEmailAddress()!=null) {
                        Attendee newElement = new Attendee();
                        newElement.setEmail(microsoftAttendee.getEmailAddress().getAddress());
                        attendeesIn.add(newElement);
                     }
                  }
                  event.setAttendees(attendeesIn);
               }

            }
            meeticalEvent.setEvent(event);
            list.add(meeticalEvent);
         }
      }
      return list;
   }

}
