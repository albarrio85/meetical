package com.meetical.service;

import com.meetical.model.MeeticalEvent;

import java.util.List;

public interface MeetingService {

   List<MeeticalEvent> getAllByUser(String user);

}
